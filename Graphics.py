#!/usr/bin/python3
import Constants
import random
import pygame
class Graphics:

    def __init__(self):
       pygame.init()

       self.screen = pygame.display.set_mode(Constants.RESOLUTION)
       self.screen.fill((255, 255, 255))
       rect = pygame.Rect(0, Constants.RESOLUTION[1]-Constants.BLOCKSIZE, Constants.RESOLUTION[0],Constants.BLOCKSIZE)
       pygame.draw.rect(pygame.display.get_surface(),(0, 0, 255), rect)
       pygame.display.flip()

       rect = pygame.Rect(0, 0, Constants.BLOCKSIZE, Constants.RESOLUTION[1])
       pygame.draw.rect(pygame.display.get_surface(),(0,0,255), rect)


       rect = pygame.Rect(Constants.RESOLUTION[0] - Constants.BLOCKSIZE, 0, Constants.BLOCKSIZE, Constants.RESOLUTION[1])
       pygame.draw.rect(pygame.display.get_surface(),(0,0,255), rect)
       self.activeShape = None
       self.play()
       #self.draw(Constants.ONEUP, 50, 50)
       #self.activeShape = [Constants.ONEUP, 50, 50, 0]
       #pygame.display.flip()
       #trash = input("")
       #self.drawRotateLeft()
       #pygame.display.flip()

    def play(self):
        numberOfRun = 0
        self.score = 0
        pygame.font.init() # you have to call this at the start,
        myfont = pygame.font.SysFont('Comic Sans MS', 30)
        oldscore = 0
        while True:
            textsurwhite = myfont.render("Score: " + str(oldscore), False, (255, 255, 255))
            textsurface = myfont.render('Score: ' + str(self.score), False, (0, 0, 0))
            oldscore = self.score
            numberOfRun += 1
            pygame.display.get_surface().blit(textsurwhite,(0,0))
            pygame.display.get_surface().blit(textsurface,(0,0))
            moveCounter = 0
            if numberOfRun == 10:
                if self.activeShape == None:
                    self.generateObject()
                self.drawMove((0,1))
                numberOfRun = 0
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.KEYDOWN and self.activeShape != None:
                    keystate = pygame.key.get_pressed()
                    if event.key == pygame.K_RIGHT:
                        if True:
                            self.drawMove((1,0))
                            moveCounter += 1
                    elif event.key == pygame.K_LEFT:
                        if True:
                            self.drawMove((-1,0))
                            moveCounter += 1
                    elif event.key == pygame.K_UP:
                        if True:
                            self.drawRotateLeft()
                            moveCounter += 1
                    elif keystate[pygame.K_DOWN]:
                            self.drawMove((0,1))
                            moveCounter += 1
            self.removeLine()
            pygame.display.flip()
            pygame.time.delay(int(50 - self.score/1000))

    def draw(self, Object, x, y, color=(0, 0, 0)):
        if not (len(Object) == 4 and len(Object[0]) == 4):
            print("Error: Bad Shape")
            exit(1)

        for i in range(len(Object)):
            for j in range(len(Object[i])):
                if(Object[i][j] == '#'):
                    self.drawBox(x+i, y+j, color)

    def drawBox(self, x, y, color=(0, 0, 0)):
        px = x * Constants.BLOCKSIZE
        py = y * Constants.BLOCKSIZE

        rect = pygame.Rect((px, py), (Constants.BLOCKSIZE, Constants.BLOCKSIZE))
        pygame.draw.rect(pygame.display.get_surface(), color,rect)

    def drawRotateLeft(self):
        self.draw(self.activeShape[0], self.activeShape[1], self.activeShape[2], (255, 255, 255))
        index = Constants.SHAPESARRAY[self.activeShape[3]].index(self.activeShape[0])

        if index == 3:
            index = 0
        else:
            index += 1

        self.activeShape[0] = Constants.SHAPESARRAY[self.activeShape[3]][index]
        self.draw(self.activeShape[0], self.activeShape[1], self.activeShape[2], (0, 0, 0))

    def removeLine(self):
        for i in range(int(Constants.RESOLUTION[1]/Constants.BLOCKSIZE) - 1):
            f=0
            for j in range(1,int(Constants.RESOLUTION[0]/Constants.BLOCKSIZE),1):
                if pygame.display.get_surface().get_at((j*Constants.BLOCKSIZE, i * Constants.BLOCKSIZE)) != (255, 0, 0, 255):
                    f = j
                    break
            if j == int(Constants.RESOLUTION[0]/Constants.BLOCKSIZE) - 1:
                for k in range(1 ,int(Constants.RESOLUTION[0]/Constants.BLOCKSIZE) -1 ):
                    self.drawBox(k, i, (255, 255, 255, 255))
                self.drawMoveWhole(i)
                self.score += 250

    def drawMoveWhole(self, toY):
        for i in range(toY, 2, -1):
            for j in range(int(Constants.RESOLUTION[0]/Constants.BLOCKSIZE)):
                color = pygame.display.get_surface().get_at((j * Constants.BLOCKSIZE, (i-1) * Constants.BLOCKSIZE))
                self.drawBox(j, i, color)


    def drawMove(self,Vect):

        for i in range(Constants.MAPSIZE):
            for j in range(Constants.MAPSIZE):
                if (self.activeShape[1] + i + 1 ) * Constants.BLOCKSIZE <= Constants.RESOLUTION[0] and (self.activeShape[1] + i ) * Constants.BLOCKSIZE >= 0:

                    if (pygame.display.get_surface().get_at(((self.activeShape[1] + i) * Constants.BLOCKSIZE, (self.activeShape[2] + j) * Constants.BLOCKSIZE)) == (0, 0, 0, 255)) and Vect[1] == 1:
                        if pygame.display.get_surface().get_at(((self.activeShape[1] + i) * Constants.BLOCKSIZE, (self.activeShape[2] + j + 1) * Constants.BLOCKSIZE)) == (255, 0, 0, 255) or  pygame.display.get_surface().get_at(((self.activeShape[1] + i) * Constants.BLOCKSIZE, (self.activeShape[2] + j + 1) * Constants.BLOCKSIZE)) == (0, 0, 255, 255):
                            self.draw(self.activeShape[0], self.activeShape[1], self.activeShape[2], (255, 0, 0))
                            self.activeShape = None
                            return

                    try:
                        if pygame.display.get_surface().get_at(((self.activeShape[1] + i) * Constants.BLOCKSIZE, (self.activeShape[2] + j) * Constants.BLOCKSIZE)) == (0, 0, 0, 255):
                            if (pygame.display.get_surface().get_at(((self.activeShape[1] + i + 1) * Constants.BLOCKSIZE ,(self.activeShape[2] + j) * Constants.BLOCKSIZE)) == (255, 0, 0, 255) or pygame.display.get_surface().get_at(((self.activeShape[1] + i + 1) * Constants.BLOCKSIZE ,(self.activeShape[2] + j) * Constants.BLOCKSIZE)) == (0, 0, 255, 255) ) and Vect[0] == 1:
                                return

                        if pygame.display.get_surface().get_at(((self.activeShape[1] + i) * Constants.BLOCKSIZE, (self.activeShape[2] + j) * Constants.BLOCKSIZE)) == (0, 0, 0, 255):
                            if (pygame.display.get_surface().get_at(((self.activeShape[1] + i -1) * Constants.BLOCKSIZE ,(self.activeShape[2] + j) * Constants.BLOCKSIZE))  == (255, 0, 0, 255) or pygame.display.get_surface().get_at(((self.activeShape[1] + i -1) * Constants.BLOCKSIZE ,(self.activeShape[2] + j) * Constants.BLOCKSIZE))  == (0, 0, 255, 255)) and Vect[0] == -1:
                                return
                    except:
                        continue

        self.draw(self.activeShape[0], self.activeShape[1], self.activeShape[2], (255, 255, 255))
        self.activeShape[1] += Vect[0]
        self.activeShape[2] += Vect[1]
        self.draw(self.activeShape[0], self.activeShape[1], self.activeShape[2], (0, 0, 0))

    def generateObject(self):
        self.activeShape = ['',0,0,0]
        pick = random.randint(0,6)
        #pick = 1
        self.activeShape[0] = Constants.SHAPESARRAY[pick][0]
        self.activeShape[1] = int((Constants.RESOLUTION[0] / Constants.BLOCKSIZE) / 2)
        self.activeShape[2] = 0
        self.activeShape[3] = pick
        self.draw(self.activeShape[0], self.activeShape[1], self.activeShape[2], (0, 0, 0))

if __name__ == "__main__":
    graph = Graphics()
